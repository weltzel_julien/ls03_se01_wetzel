import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUITest1 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUITest1 frame = new GUITest1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUITest1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHalloWelt = new JLabel("Hallo Welt");
		lblHalloWelt.setBounds(42, 30, 68, 14);
		contentPane.add(lblHalloWelt);
		
		JButton btnRed = new JButton("Red");
		btnRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonRed_clicked();
			}
		});
		btnRed.setBounds(270, 142, 89, 23);
		contentPane.add(btnRed);
		
		JButton btnBlue = new JButton("Blue");
		btnBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonBlue_clicked();
			}
		});
		btnBlue.setBounds(60, 142, 89, 23);
		contentPane.add(btnBlue);
	}

	protected void buttonRed_clicked() {
		this.contentPane.setBackground(Color.RED);
		
	}

	public void buttonBlue_clicked() {
		this.contentPane.setBackground(Color.BLUE);
		
	}

	
}
